import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { NavController, App } from 'ionic-angular';
import { ToastController,AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import 'rxjs/add/operator/map';

/*
  Generated class for the MetodosGeneralesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MetodosGeneralesProvider {

  public errorMensaje:string;

   // public UrlWebService:string=localStorage.getItem('UrlWebService');
    private datosUser;
    public array;
    public loading;

    public location : Geolocation;
    public latitud           : any;
    public longitud          : any;
    public isLocationEnabled : boolean   = false;
    public isSetLocation     : boolean   = false;

  constructor(
              public http: Http,
              public toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              public navCtrl:NavController,
              public alertCtrl: AlertController,
              public geolocation: Geolocation,
              public diagnostic: Diagnostic,
              public app:App) {


    this.datosUser = localStorage.getItem('io_dataUser');
		eval('this.datosUser='+this.datosUser);

  }

  logout(){
    let confirm = this.alertCtrl.create({
        title: 'Cerrar Sesi&oacute;n?',
        message: 'Seguro desea salir?',
        buttons: [
          {
            text: 'Si',
            handler: () => {
                localStorage.removeItem('io_dataUser');
                localStorage.removeItem('io_idUser');
                localStorage.removeItem('io_nameUser');
                localStorage.removeItem('io_idempresaUser');
                this.comprobar_login();
            }
          },
          {
             text: 'No',
              handler: () => {
                 //this.confirmedExit = false;
              }
          }
        ]
      });
      confirm.present();
}

comprobar_login(){
    if($.trim(localStorage.getItem('io_dataUser'))==''){
        this.app.getRootNav().setRoot(LoginPage);
    }
 }


 getDatosUsuarioActual(){
  return this.datosUser;
  }
  //**************FORMATEA LA HORA  */
  formateaMomento(momento) {
    const regexp = /\d\d:\d\d(:\d\d)?/;
    if (regexp.test(momento)) {
      const units = momento.split(':');
      return ((+units[0]) * 3600) + (+units[1] * 60) + (+units[2] || 0);
    }
    return null;
  }



  //Mostrar notificacion
  mostrarMensaje(  mensaje:string, tipomensaje:string='default', duracion:number = 2000, position:string='top'){
    /** Valores position: "top", "middle", "bottom" **/
    /** valores clase: rojo, amarillo, azul, default **/
    let clase = 'toast-' + tipomensaje;
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: duracion,
      cssClass: clase,
      dismissOnPageChange: true,
      position: position,
      showCloseButton:true
    });
    toast.present();
  }



  //Mostrar modal CARGANDO...
  viewLoading(text:string){
    this.loading = this.loadingCtrl.create({
        content: text
    });
    this.loading.present();
}

//Cerrar modal  CARGANDO...
closeLoading(text:string=''){
    this.loading.dismiss();
    this.loading.onDidDismiss(() => {
        //console.log(text);
    });
}




   //Setear ubicacion location, Latitud y longitud
   setLocateUser(){
    //Comprueba si la aplicación puede acceder a la ubicación del dispositivo.
//        console.log(this.diagnostic.isLocationAvailable() );;

    /* Muestra la configuración de la ubicación del dispositivo para permitir al usuario habilitar los servicios de ubicación / cambiar el modo de ubicación.
     * Android, windows
     */
//        this.diagnostic.switchToLocationSettings();

    /*
     * Muestra la configuración del dispositivo móvil para permitir al usuario habilitar los datos móviles.
     * andorid, windows
     */
//         this.diagnostic.switchToMobileDataSettings();

    /* Devuelve true si la configuración de WiFi está activada y es la misma que isWifiAvailable()
     * andorid, windows
     */
//         this.diagnostic.isWifiEnabled();
    /* Muestra la configuración WiFi para permitir al usuario habilitar WiFi.
     * andorid, windows
     */
//         this.diagnostic.switchToWifiSettings();
    this.isSetLocation = false;
    this.geolocation.getCurrentPosition()
        .then((location : any) =>
         {
             this.location          = location;
             this.latitud           = location.coords.latitude;
             this.longitud          = location.coords.longitude;
             this.isSetLocation     = true;
             return this.isSetLocation;
         })
         .catch((error : any) =>
         {
            console.log('Error getting location', error);
            this.errorMensaje  = 'Error getting location: '+error;
            this.isSetLocation = false;
            return this.isSetLocation;
         });
         return this.isSetLocation;
}



gpsDisponible(): Promise<any> {

this.isLocationEnabled = false;
return this.diagnostic.isLocationAvailable().then(

        (isAvailable) =>{
            this.isLocationEnabled = true;
            //console.log("GPS Habilitado");
        })
        .catch((error : any) =>
        {
            this.isLocationEnabled = false;
            this.errorMensaje = 'Location is:' +error;
        });
}

getCoordenadas(): Promise<any> {
this.isSetLocation = false;
return this.geolocation.getCurrentPosition()
        .then((location : any) =>
         {
             this.location          = location;
             this.latitud           = location.coords.latitude;
             this.longitud          = location.coords.longitude;
             if( this.latitud != '' && this.latitud != undefined ){
                this.isSetLocation     = true;
             }else{
                this.isSetLocation     = false;
                this.errorMensaje = ' Localizaci&oacute;n se encuentra activa';
             }
         })
         .catch((error : any) =>
         {
            this.errorMensaje  = 'Error getting location: '+error;
            this.isSetLocation = false;
         });
}

}
