import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/observable';
import { ServiceProvider } from '../service/service';


/*
  Generated class for the InicioSesionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class InicioSesionProvider {
public url;
  constructor(public http: Http,public services: ServiceProvider) {
this.url = this.services.url;
  }


  getLogin(usuario:string,password:string){

    let params=$.param({accion:'login', Usuario:$.trim(usuario), Password:$.trim(password)} );
    let user=$.trim(usuario);
    let pass=$.trim(password);

    	   // let rutasWS = new RutasWebServices();
    	let headers = new Headers({"Content-type":"application/x-www-form-urlencoded"});

      console.log(params)
      return this.http.get(this.url+'login.php?accion=login&estado=0&usuario_ap='+user+'&pass_ap='+pass, params).map(res=>res.json())
	}

}
