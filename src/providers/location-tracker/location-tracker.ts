
//import { HttpClient } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import 'rxjs/add/operator/filter';
//import { Location } from '@angular/common';
import { ServiceProvider } from '../service/service';
//import { Autostart } from '@ionic-native/autostart';
import { debounceTime } from 'rxjs/operators/debounceTime';
import { AlertController, Platform } from 'ionic-angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { BackgroundMode } from '@ionic-native/background-mode';
import {Observable} from 'rxjs/observable';
import * as moment from 'moment';
import { Device } from '@ionic-native/device';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { Socket } from 'ng-socket-io';
@Injectable()
export class LocationTrackerProvider {

  public watch: any;
  public lat: number = 0;
  public lng: number = 0;
  public info_coords: any[];
  public logs: string[] = [];
  public ultima_watch:any[] = [];
  public ultima_back: any[] = [];
  public total_metros = 1;
public primera;
public date;
public imei;
  constructor(
              public zone: NgZone,
              private backgroundGeolocation: BackgroundGeolocation,
              public geolocation: Geolocation,
              public _service: ServiceProvider,

    public db: AngularFirestore, public platform: Platform, public backgroundMode: BackgroundMode,public uniqueDeviceID: UniqueDeviceID, public socket: Socket) {
      this.socket.connect();

      this.primera = true;
      this.uniqueDeviceID.get()
  .then((uuid: any) => {this.imei = uuid;

    var res = this.imei.split("-");
    console.log(res);
    this.imei = res[3] + res[4].slice(0, -1);

    this.socket.emit('set-nickname', this.imei);
    setInterval(() => {
      this.db.collection('locations').doc(this.imei).set({
        update: moment().locale('es-CO').toLocaleString()
      }).catch(e=>{
        console.log(e)
      });
      console.log('timeOuit')
    }, 1000 * 60 * 2);
    console.log('IMEI CAMBIADO' + this.imei);
  })
  .catch((error: any) => console.log(error));


      this.date = moment();

    if (this.date.isoWeekday() !== 6 && this.date .isoWeekday() !== 7) {

      console.log('Fechas');
      console.log(this.date.isoWeekday());


    }

    //console.log('Hello LocationTrackerProvider Provider');
this.backgroundGeolocation.getLocations().then(d=>{

  d.forEach(data=>{
    var momentDate = moment.unix(data.time);
    /*var em = this.empalmar(momentDate,data.lat,data.lng);
    console.log(em);

    if (em) {
      console.log('DENTRO DEL HORARIO');

    } else {
      console.log('FUERA DEL HORARIO');

    }
 */
  })




  //*******************************CREACIÓN DEL HORARIO  */


})
    this.backgroundMode.enable();
    if (this.platform.is('android')) {
      this.backgroundMode.disableWebViewOptimizations();
    }
    this.startTracking();
    //this.autostart.enable();


    const config: BackgroundGeolocationConfig = {
      desiredAccuracy: 10,
      stationaryRadius: 20,
      distanceFilter: 30,
      debug: false, //  enable this hear sounds for background-geolocation life-cycle.
      stopOnTerminate: false, // enable this to clear background location settings when the app terminates
    };

    this.backgroundGeolocation.configure(config)
      .subscribe((location: BackgroundGeolocationResponse) => {

        // IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
        // and the background-task may be completed.  You must do this regardless if your HTTP request is successful or not.
        // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
        if (this.platform.is('ios')) {
          this.backgroundGeolocation.finish(); // FOR IOS ONLY
        }



      });

    // start recording location
    this.backgroundGeolocation.start();


  }
  ionViewDidEnter(){
    this.uniqueDeviceID.get()
.then((uuid: any) => {this.imei = uuid;

  var res = this.imei.split("-");
  console.log(res);
  this.imei = res[3] + res[4].slice(0, -1);

  this.socket.emit('set-nickname', this.imei);
  setInterval(() => {
    this.db.collection('locations').doc(this.imei).set({
      update: moment().locale('es-CO').toLocaleString()
    }).catch(e=>{
      console.log(e)
    });
    console.log('timeOuit')
  }, 1000 * 60 * 2);
  console.log('IMEI CAMBIADO' + this.imei);
})
.catch((error: any) => console.log(error));

  }
empalmar(time,lat, lng){
  if (time.isoWeekday() !== 6 && time.isoWeekday() !== 7) {
    var dat =  moment();
    if(this.primera){
      this.db.collection('locations').doc(this.imei).collection(dat.format('YYYY-M-D')).add({
        ubicaciones: [{lat: lat,lng: lng,time: moment().toISOString()}]
      }).catch(e=>{

      });
    }
    let abiertoFn = this.generadorHorario('09:00', '19:00');
    let is_time = abiertoFn(time.format('HH:mm:ss'));
    let comparacion = this.comparar(lat,lng);

    if(is_time){
      if(comparacion > this.total_metros){
              this.db.collection('locations').doc(this.imei).collection(dat.format('YYYY-M-D')).add({
                ubicaciones: [{lat: lat,lng: lng,time: moment().toISOString()}]
              }).catch(e=>{

              })
      }



      return true;
    }
  }
}
//================================== VERIFICAR HORA =================================
  //**************FORMATEA LA HORA  */
   formateaMomento(momento) {
    const regexp = /\d\d:\d\d(:\d\d)?/;
    if (regexp.test(momento)) {
      const units = momento.split(':');
      return ((+units[0]) * 3600) + (+units[1] * 60) + (+units[2] || 0);
    }
    return null;
  }


//******************************** */GENERA EL HORARIO *********************************
  generadorHorario(horaApertura, horaCierre) {

  this.formateaMomento(horaApertura);


 let tmp = this;

  return function (hora) {
    //abierto a las 00:00
    if (tmp.formateaMomento(horaApertura) > tmp.formateaMomento(horaCierre)) {
      return (tmp.formateaMomento(hora) >= tmp.formateaMomento(horaApertura) || tmp.formateaMomento(hora) <= tmp.formateaMomento(horaCierre));
    }

    return (tmp.formateaMomento(hora) >= tmp.formateaMomento(horaApertura) && tmp.formateaMomento(hora) <= tmp.formateaMomento(horaCierre));
  }

}


//=====================================================================================
    getNow(){
      let today = new Date();
      let dd = today.getDate();
      let mm = today.getMonth()+1; //January is 0!
      let hour = today.getHours();
      let minute = today.getMinutes();
      let second = today.getSeconds();

      var yyyy = today.getFullYear();

      //return dd+'/'+mm+'/'+yyyy+' '+hour+':'+minute+':'+second;
      return hour+':'+minute+':'+second;
    }



  startTracking() {

    // Background Tracking




    this.geolocation.watchPosition({enableHighAccuracy: true}).subscribe((res) => {
      // resp.coords.latitude
      // resp.coords.longitude

      var momentDate = moment(res.timestamp);
      var em = this.empalmar(momentDate,res.coords.latitude,res.coords.longitude);
      console.log(em);
      console.log(res);
      if (em) {
        console.log('DENTRO DEL HORARIO');
      } else {
        console.log('FUERA DEL HORARIO');

      }

      /*this._service.saveCoords(res.coords.latitude, res.coords.longitude).subscribe(
        data => {
          this.info_coords = data
          console.log(data)
          let alert = this.alert.create({
            title: 'Coordenadas Enviadas',
            subTitle: JSON.stringify(data),
            buttons: ['OK']
          });
          alert.present();
        },
        err => {
          console.log(err)
          let alert = this.alert.create({
            title: 'Error Coordenadas',
            subTitle: `Error: ${err}`,
            buttons: ['OK']
          });
          alert.present();
        }
      );
    */
     // console.log(`Hora: ${this.getNow()} - lat: ${res.coords.latitude} lng: ${res.coords.longitude}`)
    })

    /*this._service.saveCoords(this.lat, this.lng).subscribe(
      data=> this.info_coords = data,
      err=> console.log(err)

    );*/





    }


  // Turn ON the background-geolocation system.



  // Foreground Tracking

    // let options = {
    //   enableHighAccuracy: true
    // };

    // this.watch = this.geolocation.watchPosition(options).filter((p: any) => p.code === undefined).subscribe((position: Geoposition) => {

    // console.log(position);

    ///538 tpr

    // Run update inside of Angular's zone
    // this.zone.run(() => {
    //   this.lat = position.coords.latitude;
    //   this.lng = position.coords.longitude;
    // });
    // this._service.saveCoords(position.coords.latitude, position.coords.longitude).subscribe(
    //   data=> this.info_coords = data,
    //   err=> console.log(err)
    // );

  // });


  // const myFormattedDate = this.getNow();
  // this.logs.push(`${myFormattedDate} Geolocation: \n  Inicio`);
  // this.watch = this.geolocation.watchPosition()
  //       .pipe(debounceTime(2000)) // import relevant rxjs operator - 20 segundos : 20000 milisengundos
  //       .subscribe(position => {

  //         // Run update inside of Angular's zone
  //       this.zone.run(() => {
  //         this.lat = position.coords.latitude;
  //         this.lng = position.coords.longitude;
  //       });

  //       const myFormattedDate = this.getNow();
  //       this.logs.push(`${myFormattedDate} Geolocation: \n  ${position.coords.latitude} ${position.coords.longitude}`);
  //         console.log(`Now: ${myFormattedDate} lat: ${position.coords.latitude} lon: ${position.coords.longitude}`)


  //   });


  stopTracking() {

    console.log('stopTracking');

    this.backgroundGeolocation.finish();
    this.watch.unsubscribe();

  }

  //FUNCIÓN PARA CALCULAR DISTANCIA EN METROS

 getDistanciaMetros(lat1, lon1, lat2, lon2) {
  var rad = function (x) { return x * Math.PI / 180; }
  var R = 6378.137; //Radio de la tierra en km
  var dLat = rad(lat2 - lat1);
  var dLong = rad(lon2 - lon1);
  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(lat1)) *
    Math.cos(rad(lat2)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  //aquí obtienes la distancia en metros por la conversion 1Km =1000m
  var d = R * c * 1000;
  return d;
}
//COMPARA ACTUAL CON PASADA
comparar(lat,lng){

  console.log(this.ultima_watch);
  if(!this.primera){
    console.log('verifica:'+this.ultima_watch);
    let distancia = this.getDistanciaMetros(lat, lng, this.ultima_watch['lat'], this.ultima_watch['lng']);
    console.log('distancia:'+ distancia);
    if(distancia > this.total_metros){
      this.ultima_watch['lat'] = lat;
      this.ultima_watch['lng'] = lng;
    }else{
      return false;
    }
    return distancia;


  }
  else{
    this.ultima_watch['lat'] = lat;
    this.ultima_watch['lng'] = lng;
    this.primera = false;
    console.log('primera pasada ');
    var dat =  moment();
    console.log(this.ultima_watch);
    return true;
  }
}

}
