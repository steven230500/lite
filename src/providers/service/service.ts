import { Injectable } from '@angular/core';
import { Http, Headers} from '@angular/http';
import { Device } from '@ionic-native/device';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
interface deviceInterface {
  id?: string,
  model?: string,
  cordova?: string,
  platform?: string,
  version?: string,
  manufacturer?: string,
  serial?: string,
  isVirtual?: boolean,

};

/*
  Generated class for the ServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI. 32100009
*/
@Injectable()
export class ServiceProvider {
  public deviceInfo: deviceInterface = {};

  //url: string = 'http://demos.smartinfo.com.co/liteapp/index.php/';
  url: string = 'http://www.disuniversal.com/app_services/liteapp/';
  private imei_cel;



  constructor(public http: Http, private device: Device,public geolocation: Geolocation) {
   // console.log('Hello ServiceProvider Provider');
   this.getInfo();
   //alert('Serial: '+this.deviceInfo.serial);
  }

   // Servicio que consulta la informacion de los clientes
  getData(filtro:string, id_user: string, id_zona: string){
    let params='filtro='+filtro;

    let headers = new Headers({"Content-type":"application/x-www-form-urlencoded"});
    //return this.http.post(this.url+'Controlcliente?filtro='+filtro+'&id_usuario_ap='+id_user+'&id_zona='+id_zona, params, {headers:headers}).map(res=>res.json())
    return this.http.post(this.url+'clientes.php?accion=consultar&estado=0&filtro='+filtro+'&id_usuario='+id_user+'&id_zona='+id_zona, params, {headers:headers}).map(res=>res.json())
  }






  // Servicio que envia la informacion del pedido para ser almacenada en la bd
  url2: string = 'http://www.disuniversal.com/app_services/liteapp/';
  savePedido(strJson:string,observaciones,idUsuarioActual:string,perfil_actual,latitud:string, longitud:string){

    let params='filtro='+strJson;
    let headers = new Headers({"Content-type":"application/x-www-form-urlencoded"});
    return this.http.post(this.url2+'pedidos.php?accion=guardar&estado=0&id_usuario='+idUsuarioActual+'&filtro='+strJson+'&latitud='+latitud+'&longitud='+longitud, params, {headers:headers}).map(res=>res.json())
  }


  getRanking(id_user: string, id_zona: string){
    let params='filtro='+id_user;

    return this.http.post(this.url2+'pedidos.php?accion=ranking&estado=0&id_usuario='+id_user+'&id_zona='+id_zona, params).map(res=>res.json())
  }
  set_recaudo(id_user: string, codigoc,id_factura,id_zona: string,id_cliente,valor,filtro){
    var lat;
    var lng;
    this.geolocation.getCurrentPosition({enableHighAccuracy: true}).then((res) => {
      // resp.coords.latitude
      // resp.coords.longitude
         lat  =  res.coords.latitude;
         lng =   res.coords.longitude
         this.http.get(this.url2+'recaudos_insertar.php?accion=set_recaudo&estado=0&id_usuario='+id_user+'&codigoc='+codigoc+'&id_cliente='+id_cliente+'&valor='+valor+'&id_factura='+id_factura+'&id_zona='+id_zona+'&',{params:{filtro: filtro,lat:lat, lng: lng}}).subscribe(res=>res.json());
});


  }

  url3: string = 'http://www.disuniversal.com/app_services/liteapp/';

  saveCoords(latitud:number, longitud:number){
    this.imei_cel = localStorage.getItem('serial');
    let params='0=';
    let headers = new Headers({"Content-type":"application/x-www-form-urlencoded"});
    return this.http.post(this.url3+'pedidos.php?accion=guardarcoords&estado=0&latitud='+latitud+'&longitud='+longitud+'&imei='+this.imei_cel, params, {headers:headers}).map(res=>res.json())
  }



  getInfo() {
    this.deviceInfo.id = this.device.uuid;
    this.deviceInfo.model = this.device.model;
    this.deviceInfo.cordova = this.device.cordova;
    this.deviceInfo.platform = this.device.platform;
    this.deviceInfo.version = this.device.version;
    this.deviceInfo.manufacturer = this.device.manufacturer;
    this.deviceInfo.serial = this.device.serial;
    this.deviceInfo.isVirtual = this.device.isVirtual;

    localStorage.setItem('serial', this.deviceInfo.serial);
  }

}
