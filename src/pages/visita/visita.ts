import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { PedidosPage } from '../pedidos/pedidos';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ServiceProvider } from '../../providers/service/service';
import { File } from '@ionic-native/file';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { MetodosGeneralesProvider } from '../../providers/metodos-generales/metodos-generales';
/**
 * Generated class for the VisitaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-visita',
  templateUrl: 'visita.html',
})
export class VisitaPage {

      public idCliente:number;
      public codigoCliente: number;
      public nameCliente:string;
      public contactoCliente:string;
      public direccionCliente:string;
      public base64Image;
      public motivos:any[];
      public tipo;
      public datosUser;
      public compPedidosItem: PedidosPage;
  constructor(public navCtrl: NavController, public navParams: NavParams,public camera: Camera,public services: ServiceProvider,public transfer: FileTransfer,public loadingCtrl: LoadingController,public geolocation: Geolocation) {
    this.compPedidosItem = navParams.get('compPedidosItems');
    this.datosUser = JSON.parse(localStorage.getItem('io_dataUser'));
    console.log(this.datosUser);
    this.tipo = navParams.get('motivo');
    console.log(this.compPedidosItem);
    this.idCliente = this.compPedidosItem.idCliente;
    this.codigoCliente= this.compPedidosItem.codigoCliente;
    this.nameCliente = this.compPedidosItem.nameCliente;
    this.contactoCliente = this.compPedidosItem.contactoCliente;
    this.direccionCliente = this.compPedidosItem.direccionCliente;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VisitaPage');
    this.get_motivos();
  }
  get_motivos(){
    this.services.http.get(this.services.url+"pedidos.php?accion=get_motivos&estado=0").subscribe(data=>{
      this.motivos = data.json();
    });
  }
  evidencia(){
    const options: CameraOptions = {
          quality: 50,
          destinationType: this.camera.DestinationType.FILE_URI,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          correctOrientation:true
    }

    this.camera.getPicture(options).then((imageData) => {
         // imageData is either a base64 encoded string or a file URI
         // If it's base64 (DATA_URL):
         this.base64Image = imageData;
      }, (err) => {

    });
  }
  unico () {
// Math.random should be unique because of its seeding algorithm.
// Convert it to base 36 (numbers + letters), and grab the first 9 characters
// after the decimal.
   return '_' + Math.random().toString(36).substr(2, 9);
};
  subir(motivo){
    var imagen;
      //   let options1: FileUploadOptions = {
      //     fileKey: 'file',
      //     fileName: 'name.jpg',
      //     headers: {}
      //
      //  }
      // const fileTransfer: FileTransferObject = this.transfer.create();
      // fileTransfer.upload(this.base64Image, this.services.url+"/archivos.php", options1)
      // .then((data) => {
      //   // success
      //   alert("success");
      // }, (err) => {
      //   // error
      //   console.log(err);
      //   alert("error"+JSON.stringify(err));
      // });

      let loader = this.loadingCtrl.create({
   content: "Uploading..."
 });
 loader.present();
 if(this.base64Image){
   const fileTransfer: FileTransferObject = this.transfer.create();
   imagen = this.unico();
   let options: FileUploadOptions = {
     fileKey: 'ionicfile',
     fileName: imagen,
     chunkedMode: false,
     mimeType: "image/jpeg",
     headers: {}
   }

   fileTransfer.upload(this.base64Image, this.services.url+"/archivos.php", options)
     .then((data) => {

     //this.base64Image = "http://192.168.0.7:8080/static/images/ionicfile.jpg"
     loader.dismiss();
     swal("¡Correcto!","Subida correcta.","success");
   }, (err) => {
     console.log(err);
     loader.dismiss();
     console.log()
   });
 }
 this.geolocation.getCurrentPosition({enableHighAccuracy: true}).then((res) => {
   // resp.coords.latitude
   // resp.coords.longitude
   if(res.coords.latitude){
     this.services.http.get(this.services.url+"pedidos.php?accion=registrar_visita&estado=0",{params:{
       codigo_cliente: this.codigoCliente,
       motivo: motivo,
       imagen: imagen,
       latitud: res.coords.latitude,
       longitud: res.coords.longitude,
       tipo: this.tipo,
       vendedor: this.datosUser.id_usuario
     }}).subscribe(da=>{
       loader.dismiss();
       this.navCtrl.popAll();
     });
   }

   this.navCtrl.popAll();

 })


  }
}
