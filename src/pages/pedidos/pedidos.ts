import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, AlertController } from 'ionic-angular';

import { ArticulosProvider } from '../../providers/articulos/articulos';
import { Articulo } from '../../app/model/articulo';
import { MetodosGeneralesProvider } from '../../providers/metodos-generales/metodos-generales';
import { PedidosConfirmarPage } from '../pedidos-confirmar/pedidos-confirmar';
import { HomePage } from '../home/home';
import sweet from 'sweetalert';
import { VisitaPage } from '../visita/visita';
@IonicPage()
@Component({
  selector: 'page-pedidos',
  templateUrl: 'pedidos.html',
  providers: [MetodosGeneralesProvider]
})

export class PedidosPage {

  idCliente:number;
  codigoCliente:number;
	nameCliente:string;
	direccionCliente:string;
  contactoCliente:string;
  filtroArticulo:string = '';
  statusResp: string;
  errorMessage: string = null;
  ultimaVisita: string;

  latitud:any;
  longitud:any;

  //articulos: any[];
  public articulos: Articulo[] ;
  public articuloSel: Articulo ; //Articulo seleccionado

  public contArticulos:number = 0 ;//Contador de articulos del carrito compra
  public valorTotal:number = 0;

  //formulario de item a agrgar al pedido
	public cantidadItem:number = null;
	public desc1:number = null ;
  public desc2:number = null;
  public max_desc:number = null;

  confirmedExit:boolean=false;

  public arrayItemsPedido;

  constructor(
    public navCtrl:   NavController,
    public navParams: NavParams,
    public service:   ArticulosProvider,
    public loadingCtrl: LoadingController,
    private _metodosGenerales: MetodosGeneralesProvider,
    public alertCtrl:AlertController,
    public modalCtrl: ModalController) {

      this.articuloSel = new Articulo(0,'','descripcio articulo',0,0,0,0,'','','',0);

        this.idCliente = parseInt(navParams.get('idCliente'));
        this.codigoCliente=parseInt(navParams.get('codigoCliente'));
        this.nameCliente = navParams.get('nameCliente');
        this.direccionCliente = navParams.get('direccionCliente');
        this.contactoCliente = navParams.get('contactoCliente');
        this.ultimaVisita= navParams.get('ultimaVisita');
        this.latitud=navParams.get('latitud');
        this.longitud= navParams.get('longitud');





  }

  //utilizada para no sobrecargar el constructor con peticiones pesadas
  ngOnInit(){
    let loader = this.loadingCtrl.create();
		//loader.present();
      this.getArticulos();
    //loader.dismiss();
  }

  getArticulos(){
    //this._metodosGenerales.viewLoading('Cargando articulos');
    this.service.getDataArticulos(this.filtroArticulo, this.idCliente).subscribe(
      result=>{
        this.statusResp = result.status;
         this.articulos = result.data
         //this._metodosGenerales.closeLoading();
         if(this.statusResp=='success'){
          this.articulos = result.data;
        }
      },
      err=>{
        this.errorMessage = <any>err;
        if(this.errorMessage !== null){
          console.log(this.errorMessage);
          console.log("No se pudo conectar al servidor");
        }
      }
    );
  }



  buscarArticulo(ev: any) {
    // set val to the value of the searchbar
		let val = ev.target.value;
		this.filtroArticulo = val;

		// if the value is an empty string don't filter the items
		if (val && val.trim() != '') {
			this.getArticulos();
		  /*this.items = this.items.filter((item) => {
			return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
		  })*/
		}
  }


  ionViewCanLeave() {

}

reiniciarPedido(searchCliente:boolean=false){
  localStorage.removeItem('io_listpedido');
  this.contArticulos = 0 ;
  this.confirmedExit = true;
  if(searchCliente==true){
    this.navCtrl.pop();
  }

}

  //OBTENER LISTADO DE ARTICULOS DEL PEDIDO
	getArrayPedido(iniPedido: boolean = false){
	/*	if(iniPedido===true){//Limpia la lista de pedido y preparar una en blanco
			localStorage.removeItem('io_listpedido');
			this.contArticulos = 0 ;
		}
		let array_pedido:any;
		let arrayItemsPedido:any;
		if($.trim(localStorage.getItem('io_listpedido'))!=''){
			array_pedido = JSON.parse( localStorage.getItem('io_listpedido') );
        }else{
        	arrayItemsPedido=[];
        	array_pedido = {
               idCliente:this.idCliente,
		           codigoClientePedido: this.codigoCliente,
		           nombreCliente: this.nameCliente,
               items: arrayItemsPedido

           };
           let strJson = JSON.stringify(array_pedido);
          this.service.carrito_add()
        localStorage.setItem('io_listpedido', JSON.stringify(array_pedido) );

		}
    */
		return this.service.articulos;
	}


  form_pedidosCliente( artId:number,artCodigo:string,artDescripcion:string,artValor:number, artIva:number, artExistencia: number, artUmv: number){//SETEAR FORMULARIO PARA AGREGAR NUEVO ARTICULO AL PEDIDO

  //let array_pedido = this.getArrayPedido();

  let arrayItem = this.service.articulos;

  this.articuloSel.id = artId;
  this.articuloSel.descripcion = artDescripcion;
  this.articuloSel.codigo = artCodigo;
  this.articuloSel.precio = artValor;
  this.articuloSel.iva = artIva;
  this.articuloSel.existencia=artExistencia;
  this.articuloSel.umv=artUmv;



 /* for(var i in arrayItem){
      if( arrayItem[i].id ===  artId ){
      this._metodosGenerales.mostrarMensaje('El articulo '+this.articuloSel.descripcion+' se encuentra en el listado de pedido pendiente realizar.');
      return false;
      }
    }*/

}

isShownConfirm(cond:string){//CONDICION PARA OCULTAR Y MOSTRAR FORMULARIO ITEMS O LISTADO DE ITEM
		if(cond=="lista"){
			if(this.articuloSel.id==0){
				return true;
			}
			return false;
		}else{
			if(this.articuloSel.id > 0 ){
				return true;
			}
			return false;
		}
  }


//CANCELAR ADD DE ARTICULO AL PEDIDO
cancelarItem(){
  this.cantidadItem = null;
  this.desc1 = null ;
  this.desc2 = null;
  this.articuloSel.clearItem();
}

  getTotalItemSel( cantidad:number, precio:number, iva:number, descuento1:number=0, descuento2:number=0):number{

		if((descuento1+descuento2)>this.max_desc){
			//this._metodosGenerales.mostrarMensaje('Total descuentos no valido. ', 'amarillo');
			return 0;
		}else{
			let des1 = (cantidad * precio ) * ( descuento1 / 100 );
			let des2 = ((cantidad * precio ) * ( 1-(descuento1 / 100 ) ) ) * ( descuento2 / 100);
			let iva1 = ((cantidad * precio ) - des1 - des2 )*( iva / 100 );
      let sub_tot = (cantidad * precio ) - des1 /* - des2 + iva1*/;
     // this.valorTotal+=sub_tot;
      return sub_tot;

    }

	}


//AGREGAR ARTICULO A AL CARRITO DE COMPRA
addItemCart(){
//  let array_pedido:any = this.getArrayPedido();
  //this.arrayItemsPedido = this.service.articulos;
  if( this.validarAddItems( this.arrayItemsPedido )){
    this.arrayItemsPedido = {
          id:			    	this.articuloSel.id,
          codigo:		  	this.articuloSel.codigo,
          descripcion:	this.articuloSel.descripcion,
          cantidad: 		this.cantidadItem,
          precio: 		  this.articuloSel.precio,
          desc1:			  this.desc1,
          desc2:			  this.desc2,
          umv:          this.articuloSel.umv
         };


    //array_pedido.items = this.arrayItemsPedido;
    this.service.carrito_add(this.arrayItemsPedido);
    //localStorage.setItem('io_listpedido', JSON.stringify(array_pedido) );
    sweet("","Item Agregado","success");
    //this._metodosGenerales.mostrarMensaje('Item '+this.articuloSel.descripcion+' agregado ', 'verde');

    //this.valorTotal+=(this.cantidadItem * this.articuloSel.precio);

    this.articuloSel.clearItem();
    this.cancelarItem();
    this.contArticulos++;

  }
}

deleteItemCart(index){

  let array_pedido:any = this.getArrayPedido();
  this.arrayItemsPedido = array_pedido.items;
  //this.arrayItemsPedido.delete(index);

  this.arrayItemsPedido.splice(index, 1);
  array_pedido.items = this.arrayItemsPedido;
  localStorage.setItem('io_listpedido', JSON.stringify(array_pedido));
  sweet("","Item Eliminado","success");
  //this._metodosGenerales.mostrarMensaje('Item '+this.articuloSel.descripcion+' eliminado ', 'verde', 1000);
  this.contArticulos = this.arrayItemsPedido.length;
}


	//VALIDA EL INGRESO DE ARTICULO AL LISTADO DE PEDIDO
	validarAddItems(arrayItem):boolean{


        let mensaje:string = "";
        if( this.articuloSel.id <= 0 ){
          sweet("Error","Articulo no identificado","error");
        }
        if( this.cantidadItem <= 0 ){
          sweet("Error","Cantidad del articulo requerida","error");

        }
        if((this.cantidadItem % this.articuloSel.umv) !=0 ){
            sweet("Error","La Unidad Múltiplo de Ventas (UMV) para este articulo es "+this.articuloSel.umv,"error");

        }
        if( mensaje.length > 0 ){
         // this._metodosGenerales.mostrarMensaje(mensaje, 'amarillo');

          return false;
        }

       /* for(var i in arrayItem){
            if( arrayItem[i].id ===  this.articuloSel.id ){
            this._metodosGenerales.mostrarMensaje('El articulo '+this.articuloSel.descripcion+' se encuentra en el listado de pedido pendiente realizar.', 'azul');
            //this._metodosGenerales( 'El articulo "'+this.articuloSel.descripcion+'" se encuentra en el listado de pedido pendiente realizar.','amarillo' );
            return false;
            }
        }*/
          return true;
      }

      verFormConfirmarPedido() {
        this.navCtrl.push(PedidosConfirmarPage, { 'compPedidosItems': this })

      }
      ir_visita() {
        this.navCtrl.push(VisitaPage, { 'compPedidosItems': this ,'motivo': 'P'})

      }

  ionViewDidLoad() {

  }

}
