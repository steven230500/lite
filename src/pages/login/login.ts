import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { Login } from '../../app/model/login';
import { TabsPage } from '../tabs/tabs';
import { Observable } from 'rxjs/observable';
import { Subscription } from 'Rxjs/Subscription'

import { InicioSesionProvider } from '../../providers/inicio-sesion/inicio-sesion';
import { LocationTrackerProvider } from '../../providers/location-tracker/location-tracker';
import  swal from 'sweetalert';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public loader;
	errorMessage: string = null;
  public login:Login ;
  mensaje: string=null;
  respuesta: string=null;
  tipo_mensaje:string=null;
  datos:any;

  constructor(
          public navCtrl: NavController,
          public navParams: NavParams,
          public events: Events,
          private loadingCtrl: LoadingController,
          private alertCtrl: AlertController,
          private modalCtrl: ModalController,
          private _iniciarSesionService: InicioSesionProvider,
          public locationTracker: LocationTrackerProvider) {

          this.login = new Login('','',0);
  }

  ionviewdidenter() {
    console.log('ionViewDidLoad LoginPage');
  }


  ngOnInit(){
    // Observable.interval(5000).subscribe(()=>{
    this.start()
  //  })

	}


  start(){
    this.locationTracker.startTracking();
  }

  stop(){
    this.locationTracker.stopTracking();
  }




  Onlogin() {

    this.loader = this.loadingCtrl.create({
                  content: "Iniciando Sesion...",
                });
    this.loader.present();

    this._iniciarSesionService.getLogin(this.login.usuario,this.login.password).subscribe(
        result => {
            this.respuesta = result.respuesta;
            this.mensaje = result.mensaje;

            if(this.respuesta=='correcto'){

                this.datos = result.datos;

                localStorage.setItem('io_dataUser', JSON.stringify(this.datos));
                localStorage.setItem('io_idUser', this.datos.id_usuario);
                localStorage.setItem('io_nameUser', this.datos.nombres+' '+this.datos.apellidos);
                localStorage.setItem('io_idZona', this.datos.id_zona);
              //  localStorage.setItem('io_idempresaUser', this.login.empresa+'');
                //localStorage.setItem('dbadmin', JSON.stringify(result.dbadmin));

                this.navCtrl.push(TabsPage);
            }else{
              swal("Error",this.mensaje,"error");
           }

        this.loader.dismiss();
        //this.metodosGenerales.eliminaLoadingJX();
    },
    error =>{
        this.errorMessage = <any>error;
        if(this.errorMessage !== null){
          console.log(this.errorMessage);
          console.log("No se pudo conectar al servidor");
        }
        //this.metodosGenerales.eliminaLoadingJX();
        }
    );

  }

}
