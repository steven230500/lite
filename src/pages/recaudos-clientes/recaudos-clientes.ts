import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { MetodosGeneralesProvider } from '../../providers/metodos-generales/metodos-generales';
import sweetAlert from 'sweetalert';
import { VisitaPage } from '../visita/visita';
/**
 * Generated class for the RecaudosClientesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 interface tipo_pago {
   id?: number;
   estado: string;
 }
@Component({
  selector: 'page-recaudos-clientes',
  templateUrl: 'recaudos-clientes.html',
})
export class RecaudosClientesPage {

  /** FORMULARIO */
  public tipoAnticipo = {};
	public valorAnticipo:number = null;
	public banco_anticipo_t:number;
	public cuenta_anticipo:string;
	public anticipo_posfechado:string;
	public num_anticipo:number;
	public banco_anticipo_c:number;
	public observaciones_acticipo = {};
  public nroRecibo;
  public codigoc;
  public id_usuario;
  public id_zona;
  public id_cliente;
  public estado;
  public facturas:any[];
  public numeros_fac_model ={} ;
  public bancos: any[];
  public VisitaPage = VisitaPage;
  public datos;
  constructor(public navCtrl: NavController, public navParams: NavParams,public services:ServiceProvider) {
console.log(this.navParams.get);
this.datos = this.navParams.data;
let info = JSON.parse(localStorage.getItem('io_dataUser'));
console.log(info)
  this.codigoc = this.navParams.get('codigoCliente');
  this.id_usuario = info.id_usuario;
  this.id_zona = info.id_zona;
  this.id_cliente = this.navParams.get('idCliente');

  this.services.http.get("http://www.disuniversal.com/app_services/liteapp/recaudos.php",{params:{accion: 'getDocumentosRecaudos',codigoc: this.codigoc,id_usuario: this.id_usuario,id_zona:this.id_zona,id_cliente: this.id_cliente}}).subscribe(data=>{
   console.log(info);
    this.nroRecibo = data.json().nro_recibo;
    if(data.json().mensaje =='El cliente '+this.codigoc+' () se encuentra al d&iacute;a y no tiene facturas por Pagar.  '){
      this.estado = 0;
      sweetAlert("","El cliente "+this.codigoc+" se encuentra al día y no tiene facturas por Pagar.","info");
      this.navCtrl.pop();
    }
    else{
      this.estado = 1;
      this.facturas = data.json().documentos;
      this.nroRecibo = data.json().nro_recibo;
      this.bancos = data.json().bancos;
    }

  });
  }


  isShownConfirm(cond){//CONDICION PARA OCULTAR Y MOSTRAR FORMULARIO ITEMS O LISTADO DE ITEM
		if(cond=="C"){
			if(this.tipoAnticipo ==  cond ){
				return true;
			}
			return false;
		}else if(cond=="T"){
			if(this.tipoAnticipo ==  cond ){
				return true;
			}
			return false;
		}
		return false;
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad RecaudosClientesPage');
  }
resumen_recaudo(valor,id_factura,tipo){
  this.valorAnticipo;
  this.nroRecibo;
  let filtro;
  if(tipo == 'C'){
     filtro = {
      banco: this.banco_anticipo_c,
      num_cheque: this.num_anticipo,
      posfechado: this.anticipo_posfechado
    }
  }
    if(tipo == 'T'){
       filtro = {
        banco: this.banco_anticipo_t,
        cuenta: this.cuenta_anticipo

      }
    }
  console.log(filtro)
  //console.log(this._metodosGenerales.getDatosUsuarioActual());
  //id_user: string, codigoc,id_factura,id_zona: string,id_cliente,valor
  this.services.set_recaudo(this.id_usuario,this.codigoc,id_factura,this.id_zona,this.id_cliente,valor,filtro);
  swal('¡Correcto!','recaudo insertado','success');
  this.navCtrl.pop();
}
ir_visita() {
  this.navCtrl.push(VisitaPage, { 'compPedidosItems': this.datos ,'motivo': 'R' })

}
}
