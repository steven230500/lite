import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { MetodosGeneralesProvider } from '../../providers/metodos-generales/metodos-generales';
import { ServiceProvider } from '../../providers/service/service';
import { Socket } from 'ng-socket-io';
//import { LocationTrackerProvider } from '../../providers/location-tracker/location-tracker';

/**
 * Generated class for the InicioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inicio',
  templateUrl: 'inicio.html',
  providers: [MetodosGeneralesProvider,
              ServiceProvider]
})
export class InicioPage {

  datosUser:any;
	nameUsuarioActual:string;
	idUsuarioActual:string;
  perfilActual:string;

  statusResp: string;
  errorMessage: string = null;
  public ranking: any[];

  constructor(
              public navCtrl: NavController,
              public navParams: NavParams,
              private _metodosGenerales: MetodosGeneralesProvider,
              public service: ServiceProvider, public socket: Socket
            ) {

              this.datosUser = this._metodosGenerales.getDatosUsuarioActual();

	          	this.idUsuarioActual = this.datosUser.id_usuario;//Codigo interno
		          this.perfilActual = this.datosUser.cargo;//Codigo interno
              this.nameUsuarioActual  = this.datosUser.nombres + ' ' +this.datosUser.apellidos;

              this.ranking = [];
  }
  ionViewDidEnter(){
    this.getInfoRanking();
  }


  getInfoRanking(){
    this.service.getRanking(this.idUsuarioActual, this.datosUser.id_zona).subscribe(
      result=>{
        this.statusResp = result.status;
        // this._metodosGenerales.closeLoading();
         if(this.statusResp=='success'){
          this.ranking = result.data;
        }
      },
      err=>{
        this.errorMessage = <any>err;
        if(this.errorMessage !== null){
          console.log(this.errorMessage);
          console.log("No se pudo conectar al servidor");
        }
      }
    );
  }


  exit(){
		this._metodosGenerales.logout();
  }




  ionViewDidLoad() {
    console.log('ionViewDidLoad InicioPage');
  }
panico(){
   this.socket.emit('panico');
}

}
