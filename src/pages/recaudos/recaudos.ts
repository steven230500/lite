import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { MetodosGeneralesProvider } from '../../providers/metodos-generales/metodos-generales';
import { ServiceProvider } from '../../providers/service/service';
import { RecaudosClientesPage } from '../recaudos-clientes/recaudos-clientes';
/**
 * Generated class for the RecaudosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recaudos',
  templateUrl: 'recaudos.html',
  providers : [MetodosGeneralesProvider, ServiceProvider]
})
export class RecaudosPage {

  nameUsuarioActual:string;
	datosUser;
	idUsuarioActual:string;
  cargo:string;
  filtroCliente:string = '';
  users;

  constructor(
              public navCtrl: NavController,
              public navParams: NavParams,
              public service: ServiceProvider,
              public loadingCtrl : LoadingController,
              private _metodosGenerales: MetodosGeneralesProvider) {

                this.datosUser = this._metodosGenerales.getDatosUsuarioActual();
	            	this.cargo = this.datosUser.cargo;//Codigo interno
	            	this.nameUsuarioActual  = this.datosUser.nombres + ' ' +this.datosUser.apellidos
  }

  ngOnInit(){
		let loader = this.loadingCtrl.create();
		loader.present();
			 this.getDados();
	 loader.dismiss();
	}

  getDados(){

    this.service.getData(this.filtroCliente, this.datosUser.id_usuario, this.datosUser.id_zona).subscribe(
      data=> this.users = data,
			err=> console.log(err)

    );
  }

  buscarCliente(ev: any){
    // set val to the value of the searchbar
		let val = ev.target.value;
		this.filtroCliente = val;

		// if the value is an empty string don't filter the items
		if (val && val.trim() != '') {
			this.getDados();
		  /*this.items = this.items.filter((item) => {
			return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
		  })*/
		}
  }



	exit(){
		this._metodosGenerales.logout();
  }

  form_recaudosCliente(idCliente:number, codigoCliente: number, nombreCliente:string, contactoCliente:string, direccionCliente:string, ultimaVisita: string){
		this.navCtrl.push(RecaudosClientesPage,{
		  	idCliente: idCliente,
				codigoCliente: codigoCliente,
				nameCliente: nombreCliente,
				direccionCliente: direccionCliente,
				contactoCliente: contactoCliente,
				ultimaVisita: ultimaVisita
			}
		);
	}

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecaudosPage');
  }

}
