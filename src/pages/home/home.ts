import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { MetodosGeneralesProvider } from '../../providers/metodos-generales/metodos-generales';

import { PedidosPage } from '../pedidos/pedidos';
//import { ClienteProvider } from '../../providers/cliente/cliente';
import { AngularFirestore } from '@angular/fire/firestore';
import { LocationTrackerProvider } from '../../providers/location-tracker/location-tracker';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [MetodosGeneralesProvider]
})
export class HomePage {
  errorMessage: string = null;
	statusResp: string;
	public filtroCliente:string = '';

    users;

	nameUsuarioActual:string;
	datosUser;

	idUsuarioActual:string;
	cargo:string;

  constructor(
							public navCtrl: NavController,
							public service: ServiceProvider,
							public loadingCtrl : LoadingController,
              public _metodosGenerales: MetodosGeneralesProvider,
              public db: AngularFirestore,
              public location: LocationTrackerProvider
						) {
              this.location.startTracking();
		this.datosUser = this._metodosGenerales.getDatosUsuarioActual();
		this.cargo = this.datosUser.cargo;//Codigo interno
		this.nameUsuarioActual  = this.datosUser.nombres + ' ' +this.datosUser.apellidos


	}

ngOnInit(){

	}
	ionViewDidEnter(){
		let loader = this.loadingCtrl.create();

			 this.getDados();

  }

  getDados(){

    this.service.getData(this.filtroCliente, this.datosUser.id_usuario, this.datosUser.id_zona).subscribe(
      data=> this.users = data,
			err=> console.log('Error de conexion '+err)

    );
  }



  buscarCliente(ev: any) {
    // set val to the value of the searchbar
		let val = ev.target.value;
		this.filtroCliente = val;

		// if the value is an empty string don't filter the items
		if (val && val.trim() != '') {
			this.getDados();
		  /*this.items = this.items.filter((item) => {
			return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
		  })*/
		}
  }


  form_pedidosCliente(idCliente:number, codigoCliente: number, nombreCliente:string, contactoCliente:string, direccionCliente:string, ultimaVisita: string){
		this.navCtrl.push(PedidosPage,{
				idCliente: idCliente,
				codigoCliente: codigoCliente,
				nameCliente: nombreCliente,
				direccionCliente: direccionCliente,
				contactoCliente: contactoCliente,
				ultimaVisita: ultimaVisita
			}
		);
	}


	exit(){
		this._metodosGenerales.logout();
	}

  onCancel(event){

  }
}
