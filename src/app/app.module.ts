import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ServiceProvider } from '../providers/service/service';
import { HttpModule } from '@angular/http';
import { PedidosPage } from '../pages/pedidos/pedidos';
import { ArticulosProvider } from '../providers/articulos/articulos';
import { PedidosConfirmarPage } from '../pages/pedidos-confirmar/pedidos-confirmar';
import { InicioPage } from '../pages/inicio/inicio';
import { RecaudosClientesPage } from '../pages/recaudos-clientes/recaudos-clientes';
import { RecaudosPage } from '../pages/recaudos/recaudos';


import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { InfiniteScrollDirective } from '../directives/infinite-scroll/infinite-scroll';
import { MetodosGeneralesProvider } from '../providers/metodos-generales/metodos-generales';
import { InicioSesionProvider } from '../providers/inicio-sesion/inicio-sesion';
import { LocationTrackerProvider } from '../providers/location-tracker/location-tracker';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';

import { Autostart } from '@ionic-native/autostart';
import { Device } from '@ionic-native/device';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { Observable } from 'rxjs/observable';
import { BackgroundMode } from '@ionic-native/background-mode';
import { LocationConfigProvider } from '../providers/location-config/location-config';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { VisitaPage } from '../pages/visita/visita';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { AppUpdate } from '@ionic-native/app-update';
const config: SocketIoConfig = { url: 'http://demos.smartinfo.com.co:3001', options: {} };

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCfRVtmMPTjNRDC55srxwVS8fo7XW4DqdY",
    authDomain: "lufy-157916.firebaseapp.com",
    databaseURL: "https://lufy-157916.firebaseio.com",
    projectId: "lufy-157916",
    storageBucket: "lufy-157916.appspot.com",
    messagingSenderId: "97412536449"
  }
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PedidosPage,
    InicioPage,
    TabsPage,
    PedidosConfirmarPage,
    InfiniteScrollDirective,
    LoginPage,
    RecaudosClientesPage,
    RecaudosPage,
    VisitaPage

  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    SocketIoModule.forRoot(config),
    CurrencyMaskModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    PedidosPage,
    PedidosConfirmarPage,
    TabsPage,
    LoginPage,
    InicioPage,
    RecaudosClientesPage,
    RecaudosPage,
    VisitaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServiceProvider,
    ArticulosProvider,
    InicioSesionProvider,
    MetodosGeneralesProvider,
    Geolocation,
    Diagnostic,
    LocationTrackerProvider,
    BackgroundGeolocation,
    Autostart,
    Device,
    BackgroundMode,
    LocationConfigProvider,
    UniqueDeviceID,
    Camera,
    FileTransfer,
    File,
    AppUpdate
  ]
})
export class AppModule {}
