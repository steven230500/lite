import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { Socket } from 'ng-socket-io';
import { ServiceProvider } from '../providers/service/service';
import { AppUpdate } from '@ionic-native/app-update';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;
  public imei;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,public uniqueDeviceID: UniqueDeviceID, public socket: Socket,public services: ServiceProvider,private appUpdate: AppUpdate) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      const updateUrl = 'http://disuniversal.com/app_services/update.xml';
   this.appUpdate.checkAppUpdate(updateUrl).then(() => { console.log('Update available') });
      this.uniqueDeviceID.get()
  .then((uuid: any) => {this.imei = uuid;

    var res = this.imei.split("-");
    console.log(res);
    this.imei = res[3] + res[4].slice(0, -1);

    this.socket.emit('set-nickname', this.imei);
  })
  .catch((error: any) => console.log(error));
    });
    platform.resume.subscribe(()=>{
      this.uniqueDeviceID.get()
  .then((uuid: any) => {this.imei = uuid;

    var res = this.imei.split("-");
    console.log(res);
    this.imei = res[3] + res[4].slice(0, -1);

    this.socket.emit('set-nickname', this.imei);
  })
  .catch((error: any) => console.log(error));
    })
  }
}
